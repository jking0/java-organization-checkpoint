package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookingTest {

    @Test
    public void testParseReturnsBookingWithCorrectValues(){
        Booking actual = Booking.parse("r102-10:30am-11:00am");
        assertEquals(Booking.RoomType.Conference_Room, actual.getRoomType());
        assertEquals("102", actual.getRoomNumber());
        assertEquals("10:30am", actual.getStartTime());
        assertEquals("11:00am", actual.getEndTime());
    }
}
