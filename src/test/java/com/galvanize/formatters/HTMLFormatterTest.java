package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HTMLFormatterTest {

    @Test
    public void testHTMLFormat(){
        Booking booking = Booking.parse("c102-10:30am-11:00am");
        HTMLFormatter htmlFormatter = new HTMLFormatter();
        String actual = htmlFormatter.format(booking);
        String expected = "<dl>\n" +
                "  <dt>Type</dt><dd>Classroom</dd>\n" +
                "  <dt>Room Number</dt><dd>102</dd>\n" +
                "  <dt>Start Time</dt><dd>10:30am</dd>\n" +
                "  <dt>End Time</dt><dd>11:00am</dd>\n" +
                "</dl>";
        assertEquals(expected, actual);
    }
}
