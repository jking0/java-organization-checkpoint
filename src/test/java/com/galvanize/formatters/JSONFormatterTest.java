package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JSONFormatterTest {
    @Test
    public void testJSONFormat(){
        Booking booking = Booking.parse("c102-10:30am-11:00am");
        JSONFormatter jsonFormatter = new JSONFormatter();
        String actual = jsonFormatter.format(booking);
        String expected = "{\n" +
                "  \"type\": \"Classroom\",\n" +
                "  \"roomNumber\": 102,\n" +
                "  \"startTime\": \"10:30am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}";
        assertEquals(expected, actual);
    }
}

