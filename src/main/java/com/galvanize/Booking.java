package com.galvanize;

public class Booking {

    public enum RoomType {
        Conference_Room, Suite, Auditorium, Classroom
    }
    private final RoomType roomType;
    private final String roomNumber;
    private final String startTime;
    private final String endTime;

    public static Booking parse(String bookingCode) {
        String[] codeArr = bookingCode.split("-");
        char roomTypeChar = codeArr[0].charAt(0);
        RoomType roomType = RoomType.Conference_Room;
        switch (roomTypeChar){
            case 's':
                roomType = RoomType.Suite;
                break;
            case 'a':
                roomType = RoomType.Auditorium;
                break;
            case 'c':
                roomType = RoomType.Classroom;
                break;
        }
        String roomNumber = codeArr[0].substring(1);
        return new Booking(roomType, roomNumber, codeArr[1], codeArr[2]);
    }

    public Booking(RoomType roomType, String roomNumber, String startTime, String endTime){
        this.roomType = roomType;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    //static method parse(String) : Booking -> takes in booking code and returns Booking object
    //constructor with roomType(RoomType), roomNumber(String), startTime(String), endTime(String)
    //getters for all 4 properties

}
