package com.galvanize;

import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ApplicationTest {

    PrintStream original;
    ByteArrayOutputStream outContent;

    // This block captures everything written to System.out
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    // outContent.toString() will give you what your code printed to System.out
    @Test
    public void testGetFormatter(){
        Formatter actual = Application.getFormatter("html");
        Formatter expected = new HTMLFormatter();
        assertEquals(expected instanceof HTMLFormatter, actual instanceof HTMLFormatter);
    }

    @Test
    public void testCSV() {
        Application.main(new String[]{"a111-08:30am-11:00am", "csv"});
        String expected = "type,room number,start time,end time\nAuditorium,111,08:30am,11:00am\n";
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testHTML() {
        Application.main(new String[]{"r111-08:30am-11:00am", "html"});
        String expected = "<dl>\n" +
                "  <dt>Type</dt><dd>Conference Room</dd>\n" +
                "  <dt>Room Number</dt><dd>111</dd>\n" +
                "  <dt>Start Time</dt><dd>08:30am</dd>\n" +
                "  <dt>End Time</dt><dd>11:00am</dd>\n" +
                "</dl>\n";
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void testJSON(){
        Application.main(new String[]{"s111-08:30am-11:00am", "json"});
        String expected = "{\n" +
                "  \"type\": \"Suite\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}\n";
        assertEquals(expected, outContent.toString());
    }

}