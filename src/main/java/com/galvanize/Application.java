package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;

public class Application {
    public static void main(String[] args) {
        Formatter formatter = getFormatter(args[1]);
        System.out.println(formatter.format(Booking.parse(args[0])));
    }
    public static Formatter getFormatter(String format){
        Formatter formatter = null;
        switch (format){
            case "csv":
                formatter = new CSVFormatter();
                break;
            case "html":
                formatter = new HTMLFormatter();
                break;
            case "json":
                formatter = new JSONFormatter();
                break;
        }
        return formatter;
    }
}