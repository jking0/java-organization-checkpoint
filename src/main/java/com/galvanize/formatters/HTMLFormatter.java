package com.galvanize.formatters;

import com.galvanize.Booking;

public class HTMLFormatter implements Formatter{
    @Override
    public String format(Booking booking) {
        String roomType = booking.getRoomType().toString().replace('_', ' ');
        return String.format("<dl>\n" +
                "  <dt>Type</dt><dd>%s</dd>\n" +
                "  <dt>Room Number</dt><dd>%s</dd>\n" +
                "  <dt>Start Time</dt><dd>%s</dd>\n" +
                "  <dt>End Time</dt><dd>%s</dd>\n" +
                "</dl>", roomType, booking.getRoomNumber(), booking.getStartTime(), booking.getEndTime());
    }
}
