package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CSVFormatterTest {

    @Test
    public void testCSVFormat(){
        Booking booking = Booking.parse("r102-10:30am-11:00am");
        CSVFormatter csvFormatter = new CSVFormatter();
        String actual = csvFormatter.format(booking);
        String expected = "type,room number,start time,end time\nConference Room,102,10:30am,11:00am";
        assertEquals(expected, actual);
    }
}
