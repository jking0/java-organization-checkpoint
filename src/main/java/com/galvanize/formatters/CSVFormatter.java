package com.galvanize.formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter{
    @Override
    public String format(Booking booking) {
        String roomType = booking.getRoomType().toString().replace('_', ' ');
        return String.format("type,room number,start time,end time\n%s,%s,%s,%s", roomType, booking.getRoomNumber(), booking.getStartTime(), booking.getEndTime());
    }
}
